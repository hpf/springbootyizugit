package com.jk.service;

import com.jk.model.bootTree.BootTree;
import com.jk.model.user.User;

import java.util.List;

public interface UserService {


    List<BootTree> getBootTree();

    List<User> queryuser();

    void save(User user);

    void delete(Integer id);

    User findUserById(Integer id);

    void edit(User user);
}
