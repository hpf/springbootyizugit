<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>My JSP 'page.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<div align="center">
  		当前第${data.pageIndex}页/共${data.pageTotal }页/共${data.dataTotal}条
  		<input type="hidden" name="pageIndex" id="pageIndex"/>
	   	<button onclick="jump(1)">首页</button>
		<button onclick="jump(${data.pageIndex}-1)">上一页</button>
		<button onclick="jump(${data.pageIndex}+1)">下一页</button>
		<button onclick="jump(${data.pageTotal})">尾页</button>
		<input type="text" id="pageSize" name="pageSize" size="5" value="${data.pageSize }"/>
		<button onclick="jump(1)">跳转</button>
	</div>
  </body>
  <script type="text/javascript">
	function jump(obj){
		$('#pageIndex').val(obj)
		var pageSize = $('#pageSize').val();
		//pageSize = document.getElementById("pageSize").value;
		window.location.href=""+window.location.pathname+"?pageIndex="+obj+"&pageSize="+pageSize;
	}
</script>
</html>
