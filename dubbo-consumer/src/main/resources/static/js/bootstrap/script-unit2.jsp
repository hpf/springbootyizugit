<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<!-- 引入核心jquery -->

<!-- bootstrap  -->
<script src="../js/bootstrap/bootstrap3/js/bootstrap.js"></script>
<link rel="stylesheet" href="../js/bootstrap/bootstrap3/css/bootstrap.css"/>
<!-- 左侧导航树 --> 
<link rel="stylesheet" href="../js/bootstrap/bootstrap-treeview/bootstrap-treeview.min.css">
<script src="../js/bootstrap/bootstrap-treeview/bootstrap-treeview.min.js"></script>
<!-- 选项卡 -->
<link rel="stylesheet" href="../js/bootstrap/bootStrap-addTabs/bootstrap.addtabs.css">
<script src="../js/bootstrap/bootStrap-addTabs/bootstrap.addtabs.min.js"></script>
<!-- 表格 -->
<link rel="stylesheet" href="../js/bootstrap/bootstrap-table/bootstrap-table.css">
<script src="../js/bootstrap/bootstrap-table/bootstrap-table.js"></script>
<script src="../js/bootstrap/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<!-- 时间 -->
<script src="../js/bootstrap/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="../js/bootstrap/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<link rel="stylesheet" href="../js/bootstrap/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css">
<!-- 弹框 -->
<script src="../js/bootstrap/bootstrap-bootbox/bootbox.js"></script>
<!-- 图片上传 -->
<script src="../js/bootstrap/bootstrap-fileinput/js/fileinput.js"></script>
<script src="../js/bootstrap/bootstrap-fileinput/js/locales/zh.js"></script>
<link rel="stylesheet" href="../js/bootstrap/bootstrap-fileinput/css/fileinput.css">
</head>
<body>

</body>
</html>