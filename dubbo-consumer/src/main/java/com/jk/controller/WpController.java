package com.jk.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.model.bootTree.BootTree;
import com.jk.model.user.User;
import com.jk.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("user")
public class WpController {

    @Reference
    private UserService userService;

    //跳转页面bootStaup
    @RequestMapping("touser")
    public String touser(){
        return "user/treeList";
    }


    //树
    @RequestMapping("getBootTree")
    @ResponseBody
    public List<BootTree> getBootTree(){
        return userService.getBootTree();
    }



    //查询
    @RequestMapping("queryuser")
    public String queryuser(Model modle){
        List<User> users=userService.queryuser();
        modle.addAttribute("users",users);
        return "user/list";
    }

    //跳转新增页面
    @RequestMapping("/toAdd")
    public String toAdd() {
        return "user/Add";
    }


    //新增
    @RequestMapping("save")
    public String save(User user) {
        userService.save(user);
        return "redirect:queryuser";
    }

    @RequestMapping("delete")
    public String delete(Integer id) {
        userService.delete(id);
        return "redirect:queryuser";
    }

    //修改
    @RequestMapping("/toEdit")
    public String toEdit(Model model,Integer id) {
        User user=userService.findUserById(id);
        model.addAttribute("user", user);
        return "userEdit";
    }

    @RequestMapping("/edit")
    public String edit(User user) {
        userService.edit(user);
        return "redirect:queryuser";
    }
}
