package com.jk.mapper.user;

import com.jk.model.bootTree.BootTree;
import com.jk.model.user.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {

    List<User> queryuser();

    void save(User user);


    void delete(@Param("id") Integer id);

    User findUserById(@Param("id")Integer id);

    void edit(User user);

    List<BootTree> getBootTreeNodes(Integer pid);
}
