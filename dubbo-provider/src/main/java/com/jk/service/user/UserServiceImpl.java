package com.jk.service.user;

import com.alibaba.dubbo.config.annotation.Service;

import com.jk.mapper.user.UserMapper;
import com.jk.model.bootTree.BootTree;
import com.jk.model.user.User;
import com.jk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Service(interfaceClass = UserService.class )
@Component
public class UserServiceImpl  implements  UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<BootTree> getBootTree() {
        int pid = -1;
        List<BootTree> bootTrees = getBootTreeNodes(pid);
        return bootTrees;
    }

    private List<BootTree> getBootTreeNodes(Integer pid) {
        List<BootTree> bootTrees = userMapper.getBootTreeNodes(pid);
        for (BootTree bootTreeBean : bootTrees) {
            Integer id2 = bootTreeBean.getId();
            List<BootTree> bootTreeNode = getBootTreeNodes(id2);
            if (bootTreeNode == null || bootTreeNode.size() <= 0) {
                bootTreeBean.setSelectable(true);
            }else {
                bootTreeBean.setSelectable(false);
                bootTreeBean.setNodes(bootTreeNode);
            }
        }
        return bootTrees;
    }
    @Override
    public List<User> queryuser() {

        return userMapper.queryuser();
    }

    @Override
    public void save(User user) {
        userMapper.save(user);
    }


    @Override
    public void delete(Integer id) {
        userMapper.delete(id);
    }

    @Override
    public User findUserById(Integer id) {
        return userMapper.findUserById(id);
    }

    @Override
    public void edit(User user) {
        userMapper.edit(user);
    }




}
